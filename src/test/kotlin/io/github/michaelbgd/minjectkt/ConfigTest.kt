package io.github.michaelbgd.minjectkt

import org.junit.Before
import org.junit.Test

class ConfigTest {

    @Before
    fun init() {
        load("classpath:test.config")
    }

    @Test
    fun testConfig() {
        assert(config<String>("user.home") == System.getProperties()["user.home"])
        assert(config<Int>("a") == 5)
        assert(config<String>("b") == "test")
        assert(config<String>("c") == "test 5 test")
    }
}