package io.github.michaelbgd.minjectkt

import java.util.concurrent.ConcurrentHashMap

inline fun <reified T> fire(event: T) {
    val classes = try {
        mutableSetOf<Class<*>>(event!!::class.java)
    } catch (e: Exception) {
        mutableSetOf()
    }
    findInterfaces(T::class.java, classes)
    classes.mapNotNull { subscriptions[it] }
        .flatten()
        .filterIsInstance<Subscription<T>>()
        .sortedBy { it.priority ?: 50 }
        .forEach { it.onEvent(event = event) }
}

inline fun <reified T> listen(
    clazz: Class<T> = T::class.java,
    priority: Int? = null,
    crossinline onEvent: (T) -> Unit
): Subscription<T> =
    subscriptions.getOrPut(clazz) { mutableListOf() }.let { subList ->
        object : Subscription<T> {
            override val priority: Int? = priority
            override val subscribedEvent = clazz
            override fun onEvent(event: T) = onEvent(event)
        }.apply { subList.add(this) }
    }

fun Subscription<*>.unsubscribe() = subscriptions[subscribedEvent]?.remove(this)

val subscriptions = ConcurrentHashMap<Class<*>, MutableList<Subscription<*>>>()

interface Subscription<T> {
    fun onEvent(event: T)
    val subscribedEvent: Class<T>
    val priority: Int?
}