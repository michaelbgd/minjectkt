package io.github.michaelbgd.minjectkt

import java.util.concurrent.CountDownLatch
import java.util.concurrent.ExecutorService
import kotlin.concurrent.thread

interface Service {
    val runLevel: Int get() = 50

    fun startService() {}
    fun stopService() {}
}

fun run(blocking: Boolean = true, pool: ExecutorService? = null) {
    val allServices = injectAll<Service>()
        .groupBy { it.runLevel }
        .toSortedMap()

    allServices.forEach { (_, services) ->
        CountDownLatch(services.size).await {
            services.forEach { service ->
                pool?.execute { // async if pool is defined
                    service.startService()
                    countDown()
                } ?: run { // synchronous
                    service.startService()
                    countDown()
                }
            }
        }
    }
    val latch = CountDownLatch(1)
    Runtime.getRuntime().addShutdownHook(thread(start = false, name = "ShutdownHook") {
        allServices.values.flatten()
            .asReversed()
            .forEach { service -> service.stopService() }
        latch.countDown()
    })
    if (blocking) {
        latch.await()
    }
}

fun CountDownLatch.await(block: CountDownLatch.() -> Unit) {
    block()
    await()
}