package io.github.michaelbgd.minjectkt

import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

inline fun <reified T> inject(): T = injectOrNull() ?: throw BeanNotFound(T::class)

inline fun <reified T> injectOrNull(): T? = injectAll<T>().firstOrNull()

inline fun <reified T> injectAll(): List<T> =
    registry[T::class.java]?.getOrProduce()?.filterIsInstance(T::class.java) ?: emptyList()

class BeanNotFound(kClass: KClass<*>) : Exception("Bean for class $kClass not found")

val registry = ConcurrentHashMap<Class<*>, RegistryEntry>()

class RegistryEntry(val producers: MutableList<() -> Any> = mutableListOf()) {
    private var produced: List<Any>? = null
    fun getOrProduce(): List<Any> = produced ?: producers.map { it() }.also { produced = it }
}

interface RegisterReference {
    fun bindTo(clazz: Class<*>): RegisterReference
}

inline fun <reified T : Any> register(noinline bean: () -> T): RegisterReference {
    val bind = { clazz: Class<*>, obj: () -> T ->
        registry.getOrPut(clazz) { RegistryEntry() }.producers.add(obj)
    }
    mutableSetOf<Class<*>>().also { findInterfaces(T::class.java, it) }.forEach { bind(it, bean) }
    return object : RegisterReference {
        override fun bindTo(clazz: Class<*>): RegisterReference = apply {
            bind(clazz, bean)
        }
    }
}

fun findInterfaces(clazz: Class<*>, interfaces: MutableSet<Class<*>>) {
    var current: Class<*>? = clazz
    while (current != null) {
        if (current.name != "java.lang.Object") {
            interfaces.add(current)
        }
        current.interfaces.forEach { findInterfaces(it, interfaces) }
        interfaces.addAll(current.interfaces)
        current = current.superclass
    }
}