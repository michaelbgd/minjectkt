package io.github.michaelbgd.minjectkt

import java.io.File
import java.util.*
import java.util.regex.Pattern

val REPLACEMENT_PATTERN: Pattern = Pattern.compile("""(\$\{(.*?)})""")

inline fun <reified T> config(key: String): T? {
    val confString = configuration[key]?.toString()?.let { value ->
        StringBuffer().also { buffer ->
            val matcher = REPLACEMENT_PATTERN.matcher(value)
            while (matcher.find()) {
                matcher.appendReplacement(buffer, configuration.getProperty(matcher.group(2)))
            }
            matcher.appendTail(buffer)
        }.toString().ifEmpty { null }
    } ?: return null
    return when (T::class.java) {
        UUID::class.java -> UUID.fromString(confString)
        java.lang.Integer::class.java -> confString.toInt()
        java.lang.Boolean::class.java -> confString.toBoolean()
        else -> confString
    } as T?
}

inline fun <reified T> config(key: String, default: T): T = config(key) ?: default

val configuration: Properties = System.getProperties()
fun load(vararg files: String) = configuration.run {
    files.forEach { fileName ->
        if (fileName.startsWith("classpath:")) {
            Service::class.java.classLoader.getResources(fileName.removePrefix("classpath:"))
                .toList()
                .forEach { url ->
                    url.openStream().use { inputStream -> load(inputStream) }
                }
        } else {
            File(fileName).inputStream().use { inputStream ->
                load(inputStream)
            }
        }
    }
}
