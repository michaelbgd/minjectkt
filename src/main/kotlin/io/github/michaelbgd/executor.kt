package io.github.michaelbgd

import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicInteger

val numCores = Runtime.getRuntime().availableProcessors()
val pool = Executors.newFixedThreadPool(numCores, CountingThreadFactory("Starter"))

class CountingThreadFactory(private val namePrefix: String) : ThreadFactory {
    private val counter = AtomicInteger(0)
    override fun newThread(r: Runnable): Thread = Thread(r, namePrefix + counter.getAndIncrement())
}