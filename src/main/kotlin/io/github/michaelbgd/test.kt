package io.github.michaelbgd

import io.github.michaelbgd.minjectkt.*

fun main() {
    load(
        "./gradle.properties"
    )
    register { DummyServiceImpl() }
    register { TestServiceImpl() }
    register { Test(1) }
    register { Test(2) }
    register { Test(3) }
    register { Test(4) }

    run(pool = pool)
}

interface DummyService : Service {
    fun dummy()
}

interface TestService : Service {
    fun test()
}

data class TestEvent(val test: String = "test") : GenericEvent
data class TestEvent2(val test: String = "test") : GenericEvent
data class Test(val a: Int)

interface GenericEvent

class TestServiceImpl(
    private val configuredValue: String? = config("kotlin.code.style"),
    private val configuredIntValue: Int? = config("test"),
    private val configuredIntValue2: Int = config("test2", 5),
    private val tests: List<Test> = injectAll(),
) : TestService {
    override fun startService() {
        println(Thread.currentThread().name)
        println(tests.map { it.a })
        listen<TestEvent> {
            println("got event $it")
        }
        listen<TestEvent2> {
            println("got event2 $it")
        }
        listen<GenericEvent> {
            println("generic $it")
        }
    }

    override fun test() {
        println("testing")
        println(configuredValue)
        println(configuredIntValue)
        println(configuredIntValue2)
        fire(TestEvent())
        println("----")
        fire(TestEvent2())
    }
}

class DummyServiceImpl(
    private val testService: TestService? = inject()
) : DummyService {
    override fun startService() {
        dummy()
        fire(TestEvent2("asdf"))
    }

    override fun dummy() {
        testService?.test()
    }

    override fun stopService() {
        println("shutting down")
    }
}