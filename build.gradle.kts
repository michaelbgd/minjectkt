plugins {
    kotlin("jvm") version "1.6.0"
}

group = "io.github.michaelbgd"
version = "0.2"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("junit:junit:4.13.2")
}
